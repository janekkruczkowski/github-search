//
//  Repo.swift
//  GithubSearch
//
//  Created by Janek Kruczkowski on 20/03/2019.
//  Copyright © 2019 Janek Kruczkowski. All rights reserved.
//

import Foundation

struct Repo {
    
    private(set) var name: String
    private(set) var description: String
    private(set) var ownerLogin: String
    private(set) var ownerAvatar: String
    private(set) var license: String?
    private(set) var language: String?
    private(set) var homepage: String?
    private(set) var open_issues_count: Int
    private(set) var forks: Int
    private(set) var forks_url: String
    
}
