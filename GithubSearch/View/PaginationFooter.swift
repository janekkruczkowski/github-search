//
//  Pagination Footer.swift
//  GithubSearch
//
//  Created by Janek Kruczkowski on 21/03/2019.
//  Copyright © 2019 Janek Kruczkowski. All rights reserved.
//

import UIKit

class PaginationFooter: UIStackView {

    private let label = UILabel()
    private let aci = UIActivityIndicatorView(style: .gray)

    override init(frame: CGRect) {
        super.init(frame: frame)
        aci.startAnimating()
        label.text = "Ładowanie..."
        label.numberOfLines = 0
        self.addArrangedSubview(aci)
        self.addArrangedSubview(label)
        self.axis = .vertical
        self.alignment = .center
        self.distribution = .equalCentering
    }

    func showError(error: String) {
        label.text = error
        label.textColor = .red
        aci.isHidden = true
    }

    func reset() {
        label.text = "Ładowanie..."
        label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        aci.isHidden = false
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


}
