//
//  TitleLabel.swift
//  GithubSearch
//
//  Created by Janek Kruczkowski on 21/03/2019.
//  Copyright © 2019 Janek Kruczkowski. All rights reserved.
//

import UIKit

class TitleLabel: UILabel {

    convenience init(title: String) {
        self.init()
        self.text = title

    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.font = UIFont.systemFont(ofSize: 22, weight: .bold)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
