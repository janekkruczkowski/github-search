//
//  RepoCell.swift
//  GithubSearch
//
//  Created by Janek Kruczkowski on 20/03/2019.
//  Copyright © 2019 Janek Kruczkowski. All rights reserved.
//

import UIKit
import SnapKit

class RepoCell: UITableViewCell {
    private let nameLbl: UILabel = {
        let label = UILabel()
        return label
    }()

    private let descriptionLbl: UILabel = {
        let label = UILabel()
        label.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        label.numberOfLines = 3
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()

    }

    private func setupLayout() {
        addSubview(nameLbl)
        addSubview(descriptionLbl)

        nameLbl.snp.makeConstraints { make in
            make.top.equalTo(snp.top).offset(8)
            make.leading.equalTo(snp.leading).offset(8)
            make.trailing.equalTo(snp.trailing).offset(-8)
        }
        descriptionLbl.snp.makeConstraints { make in
            make.top.equalTo(nameLbl.snp.bottom).offset(8)
            make.leading.equalTo(snp.leading).offset(8)
            make.trailing.equalTo(snp.trailing).offset(-8)
            make.bottom.equalTo(snp.bottom).offset(-8)
        }
        descriptionLbl.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 751), for: .vertical)
    }

    func setupView(_ repo: Repo) {
        nameLbl.text = repo.name
        descriptionLbl.text = repo.description
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


}
