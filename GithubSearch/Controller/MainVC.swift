//
//  MainVC.swift
//  GithubSearch
//
//  Created by Janek Kruczkowski on 20/03/2019.
//  Copyright © 2019 Janek Kruczkowski. All rights reserved.
//

import UIKit
import SnapKit

class MainVC: UIViewController {

    private let githubService = GithubService.shared
    private var searchTask: DispatchWorkItem?
    private var isBack = false
    private var selectedLanguage = Language.all
    private var currentQuery = ""
    private var isPaginationFetching = false
    private var isLastPage = false

    // MARK: - UIViews Declaration
    private let searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: nil)

        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Nazwa Repozytorium"
        return searchController
    }()

    private let languageSegmentCtrl: UISegmentedControl = {
        let segments = ["Wszystkie", "Swift", "Objective-C"]
        let segmentControl = UISegmentedControl(items: segments)
        segmentControl.selectedSegmentIndex = 0
        return segmentControl
    }()

    private let repoTableView: UITableView = {
        let table = UITableView(frame: CGRect.zero)
        table.rowHeight = UITableView.automaticDimension
        table.estimatedRowHeight = UITableView.automaticDimension
        table.isHidden = true
        table.keyboardDismissMode = .onDrag
        return table
    }()

    private let activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: .gray)
        indicator.isHidden = true
        return indicator
    }()

    private let messageLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .center
        label.text = INITIAL_INFO
        return label
    }()

    private let tableFooter = PaginationFooter(frame: .init(x: 0, y: 0, width: 50, height: 60))

    // MARK: - ViewController Lifecycle section
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        navigationItem.title = "Wyszukiwanie"
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Powrót", style: .plain, target: nil, action: nil)

        setupSearchController()
        setupLayout()
        addTargets()
        setupTableView()

    }

    // MARK: - Setup View section
    private func setupLayout() {

        self.view.addSubview(languageSegmentCtrl)
        self.view.addSubview(repoTableView)
        self.view.addSubview(messageLabel)
        self.view.addSubview(activityIndicator)

        languageSegmentCtrl.snp.makeConstraints { make in
            make.top.equalTo(self.view.safeAreaLayoutGuide).offset(8)
            make.leading.equalTo(self.view.safeAreaLayoutGuide).offset(16)
            make.trailing.equalTo(self.view.safeAreaLayoutGuide).offset(-16)
        }

        repoTableView.snp.makeConstraints { make in
            make.top.equalTo(languageSegmentCtrl.snp.bottom).offset(16)
            make.leading.equalTo(self.view.safeAreaLayoutGuide).offset(16)
            make.trailing.equalTo(self.view.safeAreaLayoutGuide).offset(-16)
            make.bottom.equalTo(self.view)
        }

        activityIndicator.snp.makeConstraints { make in
            make.center.equalTo(self.view.safeAreaLayoutGuide)
        }

        messageLabel.snp.makeConstraints { make in
            make.centerY.equalTo(self.view.safeAreaLayoutGuide)
            make.leading.equalTo(self.view.safeAreaLayoutGuide).offset(16)
            make.trailing.equalTo(self.view.safeAreaLayoutGuide).offset(-16)
        }

    }

    private func setupSearchController() {
        navigationController?.navigationBar.prefersLargeTitles = true
        searchController.searchResultsUpdater = self
        definesPresentationContext = true
        searchController.searchBar.setValue("Anuluj", forKey: "_cancelButtonText")
        navigationItem.searchController = searchController
    }

    private func addTargets() {

        languageSegmentCtrl.addTarget(self, action: #selector(languageChanged(_:)), for: .valueChanged)
        let tap = UITapGestureRecognizer(target: self, action: #selector(endEditing))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)

    }

    private func setupTableView() {
        repoTableView.register(RepoCell.self, forCellReuseIdentifier: REPO_CELL)
        repoTableView.delegate = self
        repoTableView.dataSource = self


    }

    @objc private func endEditing() {

        self.searchController.searchBar.resignFirstResponder()

    }

    @objc private func languageChanged(_ sender: Any) {
        switch languageSegmentCtrl.selectedSegmentIndex {
        case 0:
            selectedLanguage = Language.all
            break;
        case 1:
            selectedLanguage = Language.swift
            break;
        case 2:
            selectedLanguage = Language.objc
            break;
        default:
            selectedLanguage = .all
        }

        fetchDataForChangedLanguage()

    }

    private func fetchDataForChangedLanguage() {
        if (searchController.searchBar.text != nil && searchController.searchBar.text!.count >= 3) {
            githubService.stopAllApiRequest()
            githubService.repos = []
            repoTableView.reloadData()
            sendSearchRequest(query: searchController.searchBar.text!.lowercased())
        }
    }


    private func sendSearchRequest(query: String) {
        self.messageLabel.isHidden = true
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        self.repoTableView.isHidden = true
        self.isLastPage = false
        self.currentQuery = query
        self.resetFooter()

        GithubService.shared.searchRepos(query: query, language: selectedLanguage) { [weak self] (error) in
            if error == nil {
                DispatchQueue.main.async {
                    self?.repoTableView.reloadData()
                    self?.messageLabel.isHidden = true
                    self?.repoTableView.isHidden = false
                    self?.activityIndicator.isHidden = true
                    self?.activityIndicator.stopAnimating()
                    self?.repoTableView.isHidden = false

                }
            } else {
                if error != APIError.canceled {

                    self?.activityIndicator.isHidden = true
                    self?.activityIndicator.stopAnimating()
                }
                self?.messageLabel.isHidden = false
                self?.repoTableView.isHidden = true
                self?.githubService.repos = []
                self?.messageLabel.text = GithubService.getErrorMessage(error: error!)
            }
        }

    }

// MARK: - Reset view
    private func resetSearch() {
        repoTableView.isHidden = true
        githubService.repos = []
        repoTableView.reloadData()
        self.messageLabel.isHidden = false
        self.messageLabel.text = INITIAL_INFO
        self.resetFooter()
        self.isLastPage = false
    }


    private func resetFooter() {

        self.tableFooter.reset()
        self.repoTableView.tableFooterView = tableFooter

    }
}

// MARK: - UISearchResultsUpdating Delegate
extension MainVC: UISearchResultsUpdating {

    func updateSearchResults(for searchController: UISearchController) {
        if searchController.searchBar.text != nil && currentQuery == searchController.searchBar.text!.lowercased() {
            return
        }
        if searchController.searchBar.text?.count == 0 {
            resetSearch()
        }
        self.searchTask?.cancel()
        githubService.stopAllApiRequest()
        if (searchController.searchBar.text == nil || searchController.searchBar.text!.count < 3) {
            return
        }

        let task = DispatchWorkItem { [weak self] in
            self?.sendSearchRequest(query: searchController.searchBar.text!.lowercased())
        }
        self.searchTask = task

        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.8, execute: task)


    }

    private func loadNextPage() {
        let page = githubService.getPageToFetch()
        isPaginationFetching = true
        githubService.searchRepos(query: currentQuery, language: selectedLanguage, page: page) { [weak self] (error) in
            if (error == nil) {

                DispatchQueue.main.async {

                    self?.repoTableView.reloadData()
                    self?.isPaginationFetching = false
                }
            } else if error == APIError.lastPage || error == APIError.canceled {
                if error == APIError.lastPage {
                    self?.isLastPage = true
                }
                self?.repoTableView.tableFooterView = UIView(frame: .zero)
                self?.isPaginationFetching = false
            } else {
                self?.tableFooter.showError(error: GithubService.getErrorMessage(error: error!)!)
                self?.isPaginationFetching = false
            }
        }
    }


}

// MARK: - UITableView Delegate
extension MainVC: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.githubService.repos.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: REPO_CELL, for: indexPath) as? RepoCell {

            if indexPath.row == githubService.repos.count - 1 && !isPaginationFetching && !isLastPage {
                loadNextPage()

            }
            cell.setupView(githubService.repos[indexPath.row])
            return cell
        }
        return UITableViewCell()
    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let repoVC = RepoDetailsVC()
        repoVC.setupView(repo: githubService.repos[indexPath.row])
        navigationController?.pushViewController(repoVC, animated: true)
        tableView.deselectRow(at: indexPath, animated: false)
    }


}


