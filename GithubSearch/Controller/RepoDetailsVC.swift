//
//  RepoDetailsVC.swift
//  GithubSearch
//
//  Created by Janek Kruczkowski on 20/03/2019.
//  Copyright © 2019 Janek Kruczkowski. All rights reserved.
//

import UIKit
import AlamofireImage
import SafariServices
import AuthenticationServices
import SwiftKeychainWrapper
import SnapKit


class RepoDetailsVC: UIViewController {

    private var webAuthSession: ASWebAuthenticationSession?
    private var repo: Repo!
    private var isForking = false

    // MARK: - UIViews Declaration
    private let scrollView: UIScrollView = {
        let sv = UIScrollView()
        sv.isUserInteractionEnabled = true
        sv.isScrollEnabled = true
        sv.showsHorizontalScrollIndicator = true
        return sv
    }()

    private let authorImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    private let authorLbl: UILabel = {
        let label = UILabel()
        label.text = "Autor"
        label.textColor = .gray
        label.font = UIFont.systemFont(ofSize: 20)
        return label
    }()

    private let detailsTitleLbl = TitleLabel(title: "Dane:")
    private let descriptionTitleLbl = TitleLabel(title: "Opis:")

    private let authorNameLbl: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 22, weight: .bold)
        label.numberOfLines = 0
        return label
    }()

    private let authorStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = NSLayoutConstraint.Axis.vertical
        stackView.distribution = UIStackView.Distribution.equalSpacing
        stackView.alignment = UIStackView.Alignment.leading
        stackView.spacing = 2.0
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()

    private let tableStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = NSLayoutConstraint.Axis.vertical
        stackView.distribution = UIStackView.Distribution.fill
        stackView.alignment = UIStackView.Alignment.leading
        stackView.spacing = 8.0
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()

    let descriptionLbl: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()

    private let buttonsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = NSLayoutConstraint.Axis.vertical
        stackView.spacing = 12.0
        return stackView
    }()

    private lazy var homepageBtn: RoundedButton = {
        let btn = RoundedButton(type: UIButton.ButtonType.system)
        btn.setTitle("Strona internetowa projektu", for: .normal)
        return btn
    }()

    private let forkBtn: RoundedButton = {
        let btn = RoundedButton(type: UIButton.ButtonType.system)
        btn.setTitle("Fork", for: .normal)
        return btn
    }()

    // MARK: - App lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Szczegóły"
        setupLayout()
        addTargets()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.prefersLargeTitles = false
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.prefersLargeTitles = true
    }

    // MARK: - View setup
    func setupView(repo: Repo) {
        self.repo = repo
        view.backgroundColor = .white
        let placeholder: UIImage = #imageLiteral(resourceName: "Icon")
        if let imgURL = URL(string: repo.ownerAvatar) {
            authorImage.af_setImage(withURL: imgURL, placeholderImage: placeholder)
        } else {
            authorImage.image = placeholder
        }

        authorNameLbl.text = "@" + repo.ownerLogin
        descriptionLbl.text = repo.description == "" ? "Brak" : repo.description
    }

    private func setupLayout() {

        view.addSubview(scrollView)

        authorStackView.addArrangedSubview(authorLbl)
        authorStackView.addArrangedSubview(authorNameLbl)

        scrollView.addSubview(authorStackView)
        scrollView.addSubview(authorImage)

        scrollView.addSubview(detailsTitleLbl)

        generateTable(items: [("Nazwa", repo.name), ("Język", repo.language), ("Licencja", repo.license), ("Open Issues", "\(repo.open_issues_count)"), ("Forks", "\(repo.forks)")])
        scrollView.addSubview(tableStackView)
        scrollView.addSubview(descriptionTitleLbl)
        scrollView.addSubview(descriptionLbl)

        scrollView.addSubview(buttonsStackView)
        if repo.homepage != nil && URL(string: repo.homepage!) != nil {
            buttonsStackView.addArrangedSubview(homepageBtn)
        }

        buttonsStackView.addArrangedSubview(forkBtn)

        //make constrains
        scrollView.snp.makeConstraints { make in
            make.edges.equalTo(view)

        }

        authorImage.snp.makeConstraints { make in
            make.top.equalTo(scrollView.snp.top).offset(16)
            make.leading.equalTo(self.view.safeAreaLayoutGuide).offset(16)
            make.size.equalTo(120)

        }
        authorStackView.snp.makeConstraints { make in
            make.centerY.equalTo(authorImage)
            make.leading.equalTo(authorImage.snp.trailing).offset(8)
            make.trailing.equalTo(self.view.safeAreaLayoutGuide).offset(-16)
        }

        detailsTitleLbl.snp.makeConstraints { make in
            make.top.equalTo(authorImage.snp.bottom).offset(16)
            make.leading.equalTo(self.view.safeAreaLayoutGuide).offset(16)
            make.trailing.equalTo(self.view.safeAreaLayoutGuide).offset(-16)
        }

        tableStackView.snp.makeConstraints { make in
            make.top.equalTo(detailsTitleLbl.snp.bottom).offset(8)
            make.leading.equalTo(self.view.safeAreaLayoutGuide).offset(16)
            make.trailing.equalTo(self.view.safeAreaLayoutGuide).offset(-16)
        }

        descriptionTitleLbl.snp.makeConstraints { make in
            make.top.equalTo(tableStackView.snp.bottom).offset(16)
            make.leading.equalTo(self.view.safeAreaLayoutGuide).offset(16)
            make.trailing.equalTo(self.view.safeAreaLayoutGuide).offset(-16)
        }

        descriptionLbl.snp.makeConstraints { make in
            make.top.equalTo(descriptionTitleLbl.snp.bottom).offset(8)
            make.leading.equalTo(self.view.safeAreaLayoutGuide).offset(16)
            make.trailing.equalTo(self.view.safeAreaLayoutGuide).offset(-16)
            make.bottom.equalTo(buttonsStackView.snp.top).offset(-24)
        }


        buttonsStackView.snp.makeConstraints { make in
            make.leading.equalTo(self.view.safeAreaLayoutGuide).offset(16)
            make.trailing.equalTo(self.view.safeAreaLayoutGuide).offset(-16)
            make.bottom.equalTo(scrollView.snp.bottom).offset(-32)
        }

    }

    private func generateTable(items: [Row]) {
        for (name, value) in items {
            if value != nil {

                let rowStackView: UIStackView = {
                    let stackView = UIStackView()
                    stackView.axis = NSLayoutConstraint.Axis.horizontal
                    stackView.distribution = UIStackView.Distribution.equalSpacing
                    stackView.alignment = UIStackView.Alignment.fill
                    stackView.spacing = 8.0
                    return stackView
                }()

                let nameLbl: UILabel = {
                    let label = UILabel()
                    label.textColor = .gray
                    label.text = name + ":"
                    return label
                }()
                let valueLbl: UILabel = {
                    let label = UILabel()
                    label.text = value
                    return label
                }()

                rowStackView.addArrangedSubview(nameLbl)
                rowStackView.addArrangedSubview(valueLbl)

                self.tableStackView.addArrangedSubview(rowStackView)

            }
        }

    }

    // MARK: - Add targets
    private func addTargets() {
        homepageBtn.addTarget(self, action: #selector(homepageBtnTapped(_:)), for: .touchUpInside)
        forkBtn.addTarget(self, action: #selector(forkBtnTapped(_:)), for: .touchUpInside)
    }


    @objc private func homepageBtnTapped(_ sender: UIButton!) {
        guard let urlString = repo.homepage else {
            return
        }
        if let url = URL(string: urlString) {

            let safariVC = SFSafariViewController.init(url: url)
            present(safariVC, animated: true)
        }

    }

    @objc private func forkBtnTapped(_ sender: UIButton!) {
        print("btn tapped")
        let retrievedString: String? = KeychainWrapper.standard.string(forKey: GITHUB_AUTH_KEY_NAME)
        if let token = retrievedString {
            forkRepo(token: token)

        } else {
            getAuthTokenWithWebLogin()
        }
    }

    private func forkRepo(token: String) {
        guard isForking == false else {
            return
        }
        isForking = true
        GithubService.shared.forkRepo(repoUrl: repo.forks_url, token: token) { [weak self] error in
            if error == nil {
                self?.createAlert(title: "Sukces", message: "Udało się utworzyć Fork")
            } else {
                self?.createAlert(title: ALERT_ERROR_TITLE, message: ALERT_ERROR_MESSAGE)
            }
            self?.isForking = false
        }

    }

    // MARK: - User Auth
    private func getAuthTokenWithWebLogin() {

        self.webAuthSession = ASWebAuthenticationSession.init(url: AUTH_URL!, callbackURLScheme: CALLBACK_URL_SCHEME, completionHandler: { [weak self] (callBack: URL?, error: Error?) in

            // handle auth response
            guard error == nil, let successURL = callBack else {
                self?.createAlert(title: ALERT_ERROR_TITLE, message: ALERT_ERROR_MESSAGE)
                return
            }
            let oauthToken = NSURLComponents(string: (successURL.absoluteString))?.queryItems?.filter({ $0.name == "access_token" }).first?.value

            print(oauthToken ?? "No OAuth Token")
            if let token = oauthToken {
                KeychainWrapper.standard.set(token, forKey: GITHUB_AUTH_KEY_NAME)
                self?.forkRepo(token: token)
            } else {
                self?.createAlert(title: ALERT_ERROR_TITLE, message: ALERT_ERROR_MESSAGE)
            }
        })

        self.webAuthSession?.start()
    }

    // MARK: - Helper
    private func createAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))

        self.present(alert, animated: true)
    }

}
