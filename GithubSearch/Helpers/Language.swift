//
//  Language.swift
//  GithubSearch
//
//  Created by Janek Kruczkowski on 20/03/2019.
//  Copyright © 2019 Janek Kruczkowski. All rights reserved.
//

import Foundation

enum Language:String {
    case all = "", swift = "swift", objc = "objective-c"
}
