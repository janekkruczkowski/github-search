//
//  APIError.swift
//  GithubSearch
//
//  Created by Janek Kruczkowski on 20/03/2019.
//  Copyright © 2019 Janek Kruczkowski. All rights reserved.
//

import Foundation

enum APIError: Error {
    case connectionError
    case canceled
    case empty
    case limit
    case unknown
    case lastPage
}
