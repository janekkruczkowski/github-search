//
//  Constans.swift
//  GithubSearch
//
//  Created by Janek Kruczkowski on 20/03/2019.
//  Copyright © 2019 Janek Kruczkowski. All rights reserved.
//

import Foundation

//api urls
let API_URL = "https://api.github.com"
let SEARCH_REPOS_ENDPOINT = "/search/repositories?q="

//cells
let REPO_CELL = "repoCell"

//segues
let TO_REPO_DETAILS_SEGUE = "toRepoDetailsVC"

//strings
let INITIAL_INFO = "Wpisz pierwsze trzy litery nazwy repozytorium"
let ALERT_ERROR_MESSAGE = "Operacja zakończona niepowodzeniem"
let ALERT_ERROR_TITLE = "Ups"

// auth urls
let AUTH_URL = URL(string: "https://github.com/login/oauth/authorize?client_id=2074e8d8a4a359800978&scope=public_repo")
let CALLBACK_URL_SCHEME = "pl.janekkruczkowski.git://auth"

//keychain keys
let GITHUB_AUTH_KEY_NAME = "githubToken"

//typealias
typealias CompletionHandler = (APIError?)->Void
typealias Row = (key: String, value: String?);
