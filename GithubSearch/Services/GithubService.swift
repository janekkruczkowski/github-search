//
//  GithubService.swift
//  GithubSearch
//
//  Created by Janek Kruczkowski on 20/03/2019.
//  Copyright © 2019 Janek Kruczkowski. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class GithubService {
    static let shared = GithubService();
    var repos = [Repo]()

    func searchRepos(query: String, language: Language, page: Int = 1, completed: @escaping CompletionHandler) {
        guard let query = query.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
            completed(APIError.unknown)
            return
        }
        let languageQuery = getLanguageQuery(language)

        let url = "\(API_URL)\(SEARCH_REPOS_ENDPOINT)\(query)\(languageQuery)&page=\(page)"
        Alamofire.request(url).responseJSON { response in


            if let data = response.result.value {
                if let statusCode = response.response?.statusCode, statusCode == 403 {
                    completed(APIError.limit)
                    return
                }
                let json = JSON(data)
                if json["total_count"].intValue == 0 {

                    completed(.empty)
                    return
                }
                if json["items"].arrayValue.count == 0 {
                    completed(APIError.lastPage)
                }

                if page == 1 {
                    self.repos = []
                }

                self.parseJSON(json: json, completed: completed)
            } else {
                if (response.error as NSError?)?.code == NSURLErrorCancelled {
                    completed(APIError.canceled)
                    return
                }
                if let err = response.error as? URLError, err.code == .notConnectedToInternet {
                    completed(APIError.connectionError)
                    return
                }
                completed(APIError.unknown)

            }


        }
    }


    func forkRepo(repoUrl: String, token: String, completed: @escaping CompletionHandler) {
        let headers: HTTPHeaders = [
            "Authorization": "token \(token)",
            "Accept": "application/json"
        ]
        Alamofire.request(repoUrl, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            switch response.result {
            case .success:
                if let json = response.result.value {
                    print(json)
                    completed(nil)
                } else {
                    completed(APIError.unknown)
                }
            case .failure(let error):
                completed(APIError.unknown)
                print(error)
            }


        }

    }

    func stopAllApiRequest() {
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
            sessionDataTask.forEach {
                $0.cancel()
            }
            uploadData.forEach {
                $0.cancel()
            }
            downloadData.forEach {
                $0.cancel()
            }
        }

    }

    func getPageToFetch() -> Int {
        return (self.repos.count % 30 == 0) ? (self.repos.count / 30) + 1 : (self.repos.count / 30) + 2
    }

    static func getErrorMessage(error: APIError) -> String? {
        switch error {
        case .canceled:
            return nil
        case .connectionError:
            return "Sprawdź swoje połączenie z interentem i spróbuj ponownie"
        case .empty:
            return "Nie udało się znaleźć repozytoriów spełniających kryteria"
        case .limit:
            return "Przekroczono limit wyszukiwania, spróbuj ponownie później"
        case .lastPage:
            return "Pobrano wszystkie elementy"
        case .unknown:
            return "Wystąpił bład"

        }

    }

    fileprivate func getLanguageQuery(_ language: Language) -> String {
        let languageQuery: String
        if language == .all {
            languageQuery = ""
        } else {
            languageQuery = "+language:\(language)"
        }
        return languageQuery
    }


    fileprivate func parseJSON(json: JSON, completed: @escaping CompletionHandler) {
        for (_, item): (String, JSON) in json["items"] {

            GithubService.shared.repos.append(Repo.init(name: item["name"].stringValue, description: item["description"].stringValue, ownerLogin: item["owner"]["login"].stringValue, ownerAvatar: item["owner"]["avatar_url"].stringValue, license: item["license"]["name"].string, language: item["language"].string, homepage: item["homepage"].string, open_issues_count: item["open_issues_count"].intValue, forks: item["forks"].intValue, forks_url: item["forks_url"].stringValue))
            completed(nil)
        }
    }


}
